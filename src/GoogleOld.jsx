import React, { useState } from 'react'
import GoogleLogin from 'react-google-login'
import { getBrowserInfo } from './App'

const GoogleOld = ({ onSuccess, onFailure }) => {

    const [sessions, setSessions] = useState([])
    const [socialLoginResponse, setSocialLoginResponse] = useState({})

    const [loginCredential, setLoginCredential] = useState({})
    const [userDetails, setUserDetails] = useState({})
    let browserInfo = getBrowserInfo()
    const responseGoogle = async (response) => {
        console.log(response, "");

        let formData = {
            "account_id": response.googleId,
            "login_type": "google",
            "token": response.tokenId,
            "device_id": browserInfo
        }


        setSocialLoginResponse({
            tokenId: response.tokenId,
            googleId: response.googleId
        })
        await socialLogin(formData).then((res) => res.json()).then((res) => {
            if (res.status == "400") {
                if (res.sessions && res.sessions.length > 0) {
                    setSessions(res.sessions)
                }
            } else {
                alert(res.message)
            }
        })
        return
        //do what u want
    }


    function socialLogin(formData, sessionType) {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
        };
        if (sessionType === "" || sessionType === undefined || sessionType === null) {
            return fetch(`${'https://auth.shangree.la/'}v2/subscribers/social-login`, requestOptions)
        } else {
            return fetch(`${'https://auth.shangree.la/'}v2/subscribers/social-login?type=${sessionType}`, requestOptions)
        }
    }

    async function clearSession(id) {
        // let { social_login_response } = this.state;
        let formData = {
            "account_id": socialLoginResponse.googleId,
            "login_type": "google",
            "token": socialLoginResponse.tokenId,
            "device_id": browserInfo,
            "session_id": id
        }
        await socialLogin(formData, "retry").then((res) => res.json()).then(async (res) => {
            if (res && res.access_token) {
                localStorage.setItem('ntv_u', JSON.stringify(res))
                localStorage.setItem('ntv_s', true)
                setSessions([])
                setLoginCredential(res)
                await getUserDetailByToken().then(res => res.json()).then((res) => {
                    setUserDetails(res)
                })
            } else {
                alert("something went wrong")
            }
        })
        // this.setState({
        //     showDeleteModal: !this.state.showDeleteModal
        // })
    }

    function getUserDetailByToken() {
        let _localData = localStorage.getItem('ntv_u') ? JSON.parse(localStorage.getItem('ntv_u')) : {}
        const requestOptions = {
            method: 'GET',
            headers: { Authorization: "Bearer " + _localData.access_token },



        };
        return fetch(`${'https://resources.shangree.la/'}subscriber/account/v2/namespaces/56/subscribers/-`, requestOptions)
    }

    function onFailure(err) {
        console.log(err, "@@");
    }

    console.log(userDetails, "@@userDetails");

    return (
        <div className="App">
            <div>
                <GoogleLogin
                    className={`btn btn-md btn-google-login h-100`}
                    clientId="579713148761-kj02tglr4i09toc25el7iqciunfrr8rl.apps.googleusercontent.com"
                    onSuccess={(response) => responseGoogle(response)}
                    onFailure={(error) => onFailure(error)}
                >
                    <span>Google</span>
                </GoogleLogin>
                <div id='google-login-one-tap-pompts' />
                <div>
                    {sessions?.length > 0 ?
                        sessions.map((session, index) => (
                            <div className='' style={{
                                display: "flex"
                            }}>
                                <p>
                                    <h5>
                                        Number of logged in sessions exceeded for this account. Please remove the other active device session by clicking on the Clear button.
                                    </h5>
                                    {session.device_id}
                                </p>
                                <p>
                                    <button type="" onClick={() => clearSession(session.id)}>CLEAR</button>
                                </p>
                            </div>
                        ))
                        : ""

                    }
                    {loginCredential && Object.keys(loginCredential).length > 0 &&
                        <div>
                            <p><h5>
                                Success ful login with credentials below</h5></p>
                            <table>
                                <tr>
                                    <th>Access Token</th>
                                    <th>Refresh Token</th>
                                    <th>Token Type</th>
                                    <th>Expires In</th>
                                </tr>
                                <tr>
                                    <td>{loginCredential.access_token.slice(0, 20)} ...</td>

                                    <td>{loginCredential.refresh_token.slice(0, 20)} ...</td>

                                    <td>

                                        {loginCredential.token_type}
                                    </td>

                                    <td>
                                        {loginCredential.expires_in}
                                    </td>
                                </tr>
                            </table>
                        </div>


                    }
                </div>
            </div>
        </div>
    );
}

export default GoogleOld


