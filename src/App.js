import logo from './logo.svg';
import './App.css';
import React, { useContext, useState } from 'react';
import { GoogleOAuthProvider } from '@react-oauth/google';
import { GoogleLogin } from '@react-oauth/google';
import jwt_decode from "jwt-decode";
import GoogleOld from './GoogleOld';

export function getBrowserInfo(flag) {
  var navUserAgent = navigator.userAgent;
  var browserName = navigator.appName;
  var browserVersion = '' + parseFloat(navigator.appVersion);
  var majorVersion = parseInt(navigator.appVersion, 10);
  var tempNameOffset, tempVersionOffset, tempVersion;


  if ((tempVersionOffset = navUserAgent.indexOf("Opera")) !== -1) {
    browserName = "Opera";
    browserVersion = navUserAgent.substring(tempVersionOffset + 6);
    if ((tempVersionOffset = navUserAgent.indexOf("Version")) !== -1)
      browserVersion = navUserAgent.substring(tempVersionOffset + 8);
  } else if ((tempVersionOffset = navUserAgent.indexOf("MSIE")) !== -1) {
    browserName = "Microsoft Internet Explorer";
    browserVersion = navUserAgent.substring(tempVersionOffset + 5);
  } else if ((tempVersionOffset = navUserAgent.indexOf("Chrome")) !== -1) {
    browserName = "Chrome";
    browserVersion = navUserAgent.substring(tempVersionOffset + 7);
  } else if ((tempVersionOffset = navUserAgent.indexOf("Safari")) !== -1) {
    browserName = "Safari";
    browserVersion = navUserAgent.substring(tempVersionOffset + 7);
    if ((tempVersionOffset = navUserAgent.indexOf("Version")) !== -1)
      browserVersion = navUserAgent.substring(tempVersionOffset + 8);
  } else if ((tempVersionOffset = navUserAgent.indexOf("Firefox")) !== -1) {
    browserName = "Firefox";
    browserVersion = navUserAgent.substring(tempVersionOffset + 8);
  } else if ((tempNameOffset = navUserAgent.lastIndexOf(' ') + 1) < (tempVersionOffset = navUserAgent.lastIndexOf('/'))) {
    browserName = navUserAgent.substring(tempNameOffset, tempVersionOffset);
    browserVersion = navUserAgent.substring(tempVersionOffset + 1);
    if (browserName.toLowerCase() == browserName.toUpperCase()) {
      browserName = navigator.appName;
    }
  }

  // trim version
  if ((tempVersion = browserVersion.indexOf(";")) !== -1)
    browserVersion = browserVersion.substring(0, tempVersion);
  if ((tempVersion = browserVersion.indexOf(" ")) !== -1)
    browserVersion = browserVersion.substring(0, tempVersion);
  let os_platform = navigator.platform
  let browserInfo = os_platform + " " + "|" + " " + browserName
  switch (flag) {
    case 'version':
      return browserVersion
    case 'model':
      return browserName

    default:
      return browserInfo;
  }

}
function App() {
  const [sessions, setSessions] = useState([])
  const [socialLoginResponse, setSocialLoginResponse] = useState({})

  const [loginCredential, setLoginCredential] = useState({})
  const [userDetails, setUserDetails] = useState({})
  let browserInfo = getBrowserInfo()
  const responseGoogle = async (response) => {
    console.log(response, "");
    const userObject = jwt_decode(response.credential);
    const { name, sub, picture, email, email_verified } = userObject;

    let formData = {
      "account_id": sub,
      "login_type": "google",
      "token": response.credential,
      "device_id": browserInfo
    }
    setSocialLoginResponse({
      tokenId: response.credential,
      googleId: sub
    })
    await socialLogin(formData).then((res) => res.json()).then((res) => {
      if (res.status == "400") {
        if (res.sessions && res.sessions.length > 0) {
          setSessions(res.sessions)
        }
      } else {
        alert("success")
      }

    })
    return
    //do what u want
  }


  function socialLogin(formData, sessionType) {
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(formData)
    };
    if (sessionType === "" || sessionType === undefined || sessionType === null) {
      return fetch(`${'https://auth.shangree.la/'}v2/subscribers/social-login`, requestOptions)
    } else {
      return fetch(`${'https://auth.shangree.la/'}v2/subscribers/social-login?type=${sessionType}`, requestOptions)
    }
  }

  async function clearSession(id) {
    // let { social_login_response } = this.state;
    let formData = {
      "account_id": socialLoginResponse.googleId,
      "login_type": "google",
      "token": socialLoginResponse.tokenId,
      "device_id": browserInfo,
      "session_id": id
    }
    await socialLogin(formData, "retry").then((res) => res.json()).then(async (res) => {
      if (res && res.access_token) {
        localStorage.setItem('ntv_u', JSON.stringify(res))
        localStorage.setItem('ntv_s', true)
        setSessions([])
        setLoginCredential(res)
        await getUserDetailByToken().then(res => res.json()).then((res) => {
          setUserDetails(res)
        })
      } else {
        alert("something went wrong")
      }
    })
    // this.setState({
    //     showDeleteModal: !this.state.showDeleteModal
    // })
  }

  function getUserDetailByToken() {
    let _localData = localStorage.getItem('ntv_u') ? JSON.parse(localStorage.getItem('ntv_u')) : {}
    const requestOptions = {
      method: 'GET',
      headers: { Authorization: "Bearer " + _localData.access_token },



    };
    return fetch(`${'https://resources.shangree.la/'}subscriber/account/v2/namespaces/56/subscribers/-`, requestOptions)
  }

  function onFailure(err) {
    console.log(err, "@@");
  }

  console.log(userDetails, "@@userDetails");

  return (
    <div className="App">
      <div>
        <GoogleOAuthProvider
          // clientId={"950296576512-fcd5djgude5i3gs21d71ccn4e69l3trd.apps.googleusercontent.com"}
          // clientId={"709142454424-vue826ekh8r8j54vaj1oortr90q6k4mn.apps.googleusercontent.com"}
          // clientId={"579713148761-kj02tglr4i09toc25el7iqciunfrr8rl.apps.googleusercontent.com"}
          clientId={"650194671128-4tf5ucmgucvdmmjfqfihtuk8cospormr.apps.googleusercontent.com"}
        >
          <GoogleLogin
            onSuccess={credentialResponse => {
              responseGoogle(credentialResponse)
            }}
            onError={(credentialResponse) => {
              responseGoogle(credentialResponse)
            }}
            theme="outline"
            size='medium'
            text='signin_with'
            width="100"
            //use this if automatically google ask user to login from their logged account with pop up open at right top of the browser
            useOneTap
          // prompt_parent_id="google-login-one-tap-pompts"
          // cancel_on_tap_outside
          />
        </GoogleOAuthProvider>
        <div id='google-login-one-tap-pompts' />
        <div>
          {sessions?.length > 0 ?
            sessions.map((session, index) => (
              <div className='' style={{
                display: "flex"
              }}>
                <p>
                  <h5>
                    Number of logged in sessions exceeded for this account. Please remove the other active device session by clicking on the Clear button.
                  </h5>
                  {session.device_id}
                </p>
                <p>
                  <button type="" onClick={() => clearSession(session.id)}>CLEAR</button>
                </p>
              </div>
            ))
            : ""
          }
          {loginCredential && Object.keys(loginCredential).length > 0 &&
            <div>
              <p><h5>
                Success ful login with credentials below</h5></p>
              <table>
                <tr>
                  <th>Access Token</th>
                  <th>Refresh Token</th>
                  <th>Token Type</th>
                  <th>Expires In</th>
                </tr>
                <tr>
                  <td>{loginCredential.access_token.slice(0, 20)} ...</td>

                  <td>{loginCredential.refresh_token.slice(0, 20)} ...</td>

                  <td>

                    {loginCredential.token_type}
                  </td>

                  <td>
                    {loginCredential.expires_in}
                  </td>
                </tr>
              </table>
            </div>
          }
        </div>
      </div>
    </div>
  );
}

export default App;
